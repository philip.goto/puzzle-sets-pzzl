# Crosswords Puzzle Set for pzzl.net

This repo contains Dutch puzzle set downloaders for the app [Crosswords](https://gitlab.gnome.org/jrb/crosswords). The puzzles are pulled from pzzl.net and converted to ipuz format supported by Crosswords. Right now this puzzle set contains both mini and large puzzles published on [De Telegraaf](https://www.telegraaf.nl/puzzels).
